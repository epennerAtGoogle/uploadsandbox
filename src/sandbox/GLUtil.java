/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sandbox;

import android.opengl.GLUtils;
import android.util.Log;
import android.view.Surface;

import java.nio.IntBuffer;
import java.nio.*;
import java.util.Arrays;
import java.util.Random;

import junit.framework.Assert;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;


import static android.opengl.GLES20.*;

public class GLUtil {

    private static Random mRandom = new Random(System.currentTimeMillis());

    private static int mNextInt = 0;
    public static int nextIndex(int range) {
        return Math.abs(++mNextInt) % range;
    }

    public static int randomIndex(int range) {
        return Math.abs(mRandom.nextInt()) % range;
    }

    public static void setCommonState() {
        glDisable(GL_CULL_FACE);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        glDepthMask(false);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glPixelStorei(GL_PACK_ALIGNMENT, 4);
    }

    public static void uploadWithTexImage(int texture, int width, int height, IntBuffer buffer) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer.position(0));
    }

    public static void uploadWithTexSubImage(int texture, int width, int height, IntBuffer buffer) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer.position(0));
    }

    public static void uploadWithTexSubImage(int texture, int width, int height, int r, int g, int b, int a) {
        nativeuploadWithTexSubImage(texture, width, height, r, g, b, a);
    }

    public static void uploadWithTexSubImage(int texture, int width, int height) {
        nativeuploadWithTexSubImage(texture, width, height, mRandom.nextInt(),
                                                            mRandom.nextInt(),
                                                            mRandom.nextInt(),
                                                            mRandom.nextInt());
    }

    public static void initTexture(int texture, int width, int height) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, null);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    public static void initTextureFromEglImage(int texture, int eglImage) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        bindEglImageToOpenglTexture2d(eglImage);
    }

    public static IntBuffer createFilledPixelBuffer(int width, int height) {
        int[] pixelData = new int[width * height];
        // Fill with a random color.
        Arrays.fill(pixelData, mRandom.nextInt());
        // Fill with a gradient.
        //int offset = 64; //randomIndex(10) * 10;
        //for(int j = 0; j < height; j++)
        //    for(int k = 0; k < width; k++)
        //        pixelData[j*width+k] = (j+k)/3 + offset;

        IntBuffer buffer = ByteBuffer.allocateDirect(pixelData.length * 4)
                           .order(ByteOrder.nativeOrder()).asIntBuffer();
        buffer.put(pixelData).position(0);
        return buffer;
    }

    public static FloatBuffer createUnitQuadVertBuffer() {
        final float[] quadVertData = {
                0.0f, 0.0f,   1.0f, 1.0f,   0.0f, 1.0f, 
                0.0f, 0.0f,   1.0f, 0.0f,   1.0f, 1.0f };
        FloatBuffer buffer = ByteBuffer.allocateDirect(quadVertData.length * 4)
                            .order(ByteOrder.nativeOrder()).asFloatBuffer();
        buffer.put(quadVertData).position(0);
        return buffer;
    }


/*
    private void initFbo(int index) {
        glBindFramebuffer(GL_FRAMEBUFFER, mFbos[index]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mTextures[index], 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, mScreenWidth, mScreenHeight);
    }
*/

    public static int createEglImageFromTexture(int texture) {
        return nativeCreateEglImageFromTexture(texture);
    }

    public static void destroyEglImage(int eglImage) {
        nativeDestroyEglImage(eglImage);
    }

    public static void bindEglImageToOpenglTexture2d(int eglImage) {
        nativeBindEglImageToOpenglTexture2d(eglImage);
    }

    public static void uploadToSurface(Surface surface, int width, int height) {
        nativeUploadToSurface(surface, width, height, mRandom.nextInt(),
                                                      mRandom.nextInt(),
                                                      mRandom.nextInt(),
                                                      mRandom.nextInt());
    }

    public static void uploadToSurface(Surface surface, int width, int height, int r, int g, int b, int a) {
        nativeUploadToSurface(surface, width, height, r, g, b, a);
    }

    public static int createSync() {
        return nativeCreateSync();
    }

    public static int waitSync(int sync) {
        return nativeWaitSync(sync);
    }

    public static int destroySync(int sync) {
        return nativeDestroySync(sync);
    }

    public static long getCpuTime() {
        return nativeGetCpuTime();
    }

    public static long getClockTime() {
        return System.nanoTime();
    }

    public static int createPbo() {
        return nativeCreatePbo();
    }

    private static native long nativeGetCpuTime();
    private static native int nativeCreateEglImageFromTexture(int texture);
    private static native int nativeDestroyEglImage(int eglImage);
    private static native int nativeBindEglImageToOpenglTexture2d(int eglImage);
    private static native int nativeuploadWithTexSubImage(int texture, int width, int height, int r, int g, int b, int a);
    private static native int nativeUploadToSurface(Surface surface, int width, int height, int r, int g, int b, int a);

    private static native int nativeCreateSync();
    private static native int nativeWaitSync(int sync);
    private static native int nativeDestroySync(int sync);

    private static native int nativeCreatePbo();
}
