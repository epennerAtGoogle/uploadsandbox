/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sandbox;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.SurfaceHolder;

import android.opengl.GLUtils;
import android.util.Log;

import java.lang.Thread;
import java.util.concurrent.Semaphore;

import junit.framework.Assert;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

import static android.opengl.GLES20.*;

import java.util.Random;

public class GLProducerThread extends GLThread {
    private GLFrameRenderer mRenderer;
    private final Semaphore mSemaphore;

    GLProducerThread(GLFrameRenderer renderer, SurfaceHolder surfaceHolder, Semaphore semaphore) {
        mRenderer = renderer;
        mSemaphore = semaphore;
        mSurfaceHolder = surfaceHolder;
    }

    @Override
    public void run() {
        //GLThread otherContext = new GLThread();
        //otherContext.initGL();

        initGL();

        int[] width = new int[1];
        int[] height = new int[1];
        mEgl.eglQuerySurface(mEglDisplay, mEglSurface, mEgl.EGL_WIDTH, width);
        mEgl.eglQuerySurface(mEglDisplay, mEglSurface, mEgl.EGL_HEIGHT, height);

        mRenderer.init(width[0], height[0]);
        while (!mRenderer.isFinished()) {
            mRenderer.renderFrame();

            //otherContext.makeContextCurrent();
            //this.makeContextCurrent();

            mEgl.eglSwapBuffers(mEglDisplay, mEglSurface);
        }
        mRenderer.shutdown();

        mSemaphore.release();
        destroyGL();
    }
}
