/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sandbox;

import static android.opengl.GLES20.*;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.egl.EGLDisplay;

import junit.framework.Assert;
import java.nio.*;
import java.util.Arrays;
import java.util.concurrent.locks.LockSupport;
import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.Surface;

public class GLTextureUploadRenderer implements GLFrameRenderer{

    public enum UploadType {EglImage, SurfaceTexture};

    static final String TAG = "GLTextureUploadRenderer";
    static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    private int mScrollOffset;

    private int mTileWidth;
    private int mTileHeight;

    private int mScreenWidth;
    private int mScreenHeight;
    private int mTileCountX;
    private int mTileCountY;

    private int mFramesRendered;
    private int mFramesToRender;
    private int mLayers;
    private int mUploadsPerFrame;
    private boolean mRecycleTextures;
    private UploadType mUploadType;

    private int mTextureCount = 0;
    private int[] mTextures;
    private int[] mEglImages;
    private Surface[] mSurfaces;
    private SurfaceTexture[] mSurfaceTextures;
    private int[] mSurfaceTextureTextures;

    private int mOffsetTexture;
    private int mThreadedOffsetTexture;
    private int mThreadedOffsetEglImage;

    private IntBuffer[] mPixelBuffers;
    private int mNextPixelBuffer;

    private FloatBuffer mQuadVertBuffer;
    private Program mProgram2D;
    private Program mProgramExternal;

    public GLUploadThread mUploadThread;

    private FrameStats mUpdateTexImageStats = new FrameStats();
    private FrameStats mUpdateTexImageBatchStats = new FrameStats();
    private FrameStats mReleaseTexImageStats = new FrameStats();
    private FrameStats mReleaseTexImageBatchStats = new FrameStats();

    private FrameStats mAttachStats = new FrameStats();
    private FrameStats mAttachBatchStats = new FrameStats();
    private FrameStats mDetachStats = new FrameStats();
    private FrameStats mDetachBatchStats = new FrameStats();

    private FrameStats mDrawStats = new FrameStats();
    private FrameStats mDrawBatchStats = new FrameStats();

    private FrameStats mUploadStats = new FrameStats();

    private FrameStats mFrameStats = new FrameStats();

    GLTextureUploadRenderer(int frames,
                            int layers,
                            int tileWidth,
                            int tileHeight,
                            int texturesToUpload,
                            boolean recycleTextures,
                            UploadType uploadType) {
        mFramesToRender = frames;
        mLayers = layers;
        mTileWidth = tileWidth;
        mTileHeight = tileHeight;
        mUploadsPerFrame = texturesToUpload;
        mRecycleTextures = recycleTextures;
        mUploadType = uploadType;
    }

    @Override
    public void init(int width, int height) {

        GLUtil.setCommonState();

        // 25MB of textures for testing.
        int budgetBytes = 1024 * 1024 * 50;
        int textureBytes = mTileWidth * mTileHeight * 4;
        mTextureCount = budgetBytes / textureBytes;

        Log.e(TAG, "TextureCount " + mTextureCount);

        // Worst case, tiles straddling viewport
        mScreenWidth = width;
        mScreenHeight = height;
        mTileCountX = (mScreenWidth + mTileWidth)  / mTileWidth + 1;
        mTileCountY = (mScreenHeight + mTileHeight) / mTileHeight + 1;

        initPixelBuffers();
        initOffsetTexture();

        mQuadVertBuffer = GLUtil.createUnitQuadVertBuffer();

        // Create shader programs
        mProgram2D = new Program(mVertexShader, mFragmentShader2D, GL_TEXTURE_2D);
        mProgramExternal = new Program(mVertexShader, mFragmentShaderExternal, GL_TEXTURE_EXTERNAL_OES);

        GLCHK();

        mUploadThread = new GLUploadThread(mTileWidth, mTileHeight);
        mUploadThread.start();
    }

    void initTextures() {
        if (mTextures != null)
            return;
        mTextures = new int[mTextureCount];
        glGenTextures(mTextureCount, mTextures, 0);
        for (int i = 0; i < mTextureCount; i++) {
            GLUtil.initTexture(mTextures[i], mTileWidth, mTileWidth);
            GLUtil.uploadWithTexImage(mTextures[i], mTileWidth, mTileWidth,
                                      mPixelBuffers[GLUtil.randomIndex(mPixelBuffers.length)]);
        }
    }

    void uploadToTextures() {
        for (int i = 0; i < mTextureCount; i++) {
            mUploadStats.startFrame();
            GLUtil.uploadWithTexImage(mTextures[i], mTileWidth, mTileWidth,
                                      mPixelBuffers[GLUtil.randomIndex(mPixelBuffers.length)]);
            mUploadStats.endFrame();
        }
    }

    void initOffsetTexture() {
        int[] texArray = new int[2];
        glGenTextures(2, texArray, 0);
        mOffsetTexture = texArray[0];
        mThreadedOffsetTexture = texArray[1];
        GLUtil.initTexture(mOffsetTexture, 1, 1);
        GLUtil.initTexture(mThreadedOffsetTexture, 1, 1);
        GLUtil.uploadWithTexSubImage(mOffsetTexture, 1, 1, 0, 0, 0, 0);
        GLUtil.uploadWithTexSubImage(mThreadedOffsetTexture, 1, 1, 0, 0, 0, 0);
        mThreadedOffsetEglImage = GLUtil.createEglImageFromTexture(mThreadedOffsetTexture);
    }

    void destroyTextures() {
        if (mTextures == null)
            return;
        glDeleteTextures(mTextures.length, mTextures, 0);
        mTextures = null;
    }

    void initEglImages() {
        if (mEglImages != null)
            return;
        mEglImages = new int[mTextureCount];
        for (int i = 0; i < mTextureCount; i++) {
            mEglImages[i] = GLUtil.createEglImageFromTexture(mTextures[i]);
        }
    }

    void destroyEglImages() {
        if (mEglImages == null)
            return;
        for (int i = 0; i < mTextureCount; i++)
            GLUtil.destroyEglImage(mEglImages[i]);
        mEglImages = null;
    }

    void initSurfaceTextures() {
        if (mSurfaceTextures != null)
            return;
        mSurfaceTextureTextures = new int[mTextureCount];
        glGenTextures(mTextureCount, mSurfaceTextureTextures, 0);
        mSurfaceTextures = new SurfaceTexture[mTextureCount];
        mSurfaces = new Surface[mTextureCount];
        for (int i = 0; i < mTextureCount; i++) {
            boolean singleBuffer = false;
            mSurfaceTextures[i] = new SurfaceTexture(mSurfaceTextureTextures[i], singleBuffer);
            mSurfaceTextures[i].setDefaultBufferSize(1, 1);
            mSurfaces[i] = new Surface(mSurfaceTextures[i]);           
            GLUtil.uploadToSurface(mSurfaces[i], mTileWidth, mTileHeight);
            mSurfaceTextures[i].updateTexImage();

            // Detach the texture from GL. This deletes the texture so
            // we set it to zero and glGenTextures when we attach again.           
            //mSurfaceTextures[i].detachFromGLContext();
            //mTextures[i] = 0;

            Log.e(TAG, "Created Surface Texture: " + i);
        }
    }

    void destroySurfaceTextures() {
        if (mSurfaceTextures == null)
            return;
        for (int i = 0; i < mTextureCount; i++) {
            mSurfaceTextures[i].release();
            mSurfaces[i].release();
        }
        mSurfaceTextures = null;
        mSurfaces = null;
    }

    void initPixelBuffers() {
        // Allocate buffers for some raw pixel data.
        mPixelBuffers = new IntBuffer[10];
        for (int i = 0; i < mPixelBuffers.length; i++)
            mPixelBuffers[i] = GLUtil.createFilledPixelBuffer(mTileWidth, mTileWidth);
    }

    @Override
    public void shutdown() {
        try {
            mUploadThread.join();
        } catch (InterruptedException e) {
        }
        Assert.assertTrue(glGetError() ==  GL_NO_ERROR);
    }

    private void drawTile(Program program, int texture, int offsetTexture, int screenWidth, int screenHeight, int offsetX, int offsetY) {
        glUseProgram(program.mProgram);

        glVertexAttribPointer(program.mCoordHandle, 2, GL_FLOAT, false, 8, mQuadVertBuffer);
        glEnableVertexAttribArray(program.mCoordHandle);
        glUniform1i(program.mTextureHandle, 0);
        glUniform2f(program.mScreenSizeHandle, screenWidth, screenHeight);
        glUniform2f(program.mTileSizeHandle, mTileWidth, mTileHeight);
        glUniform2f(program.mOffsetHandle, offsetX, offsetY);

        glUniform1i(program.mOffsetTextureHandle, 1);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, offsetTexture);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(program.mTextureTarget, texture);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }

    public void LogBatchStats(String name,  FrameStats stats, FrameStats batchStats) {
       if (batchStats.mFrameCount == 0)
           return;
       stats.logStats(name + " Stats");
       batchStats.logStats(name + " Batch Stats (for "
                                + (stats.mFrameCount / batchStats.mFrameCount)
                                + " textures)");
    }

    @Override
    public boolean isFinished() {
        if (!mUploadThread.isAlive()) {
            mFrameStats.logStats("Full Frame Stats");
            //mDrawStats.logStats("Draw stats");
            LogBatchStats("Draw",   mDrawStats, mDrawBatchStats);
            LogBatchStats("Attach", mAttachStats, mAttachBatchStats);
            LogBatchStats("Detach", mDetachStats, mDetachBatchStats);
        }
        return !mUploadThread.isAlive();
    }

    private int textureIndex(int tileX, int tileY, int tileZ) {
        return (tileX + tileY * mTileCountX + tileZ * mTileCountX * mTileCountY) % mTextures.length;
    }

    interface TileAction {
        void DoTile(int i, int j, int k, int textureIndex);
    }

    private TileAction mDrawTile = new TileAction() {
        public void DoTile(int i, int j, int k, int textureIndex) {
            mDrawStats.startFrame();
            Program program = (mUploadType == UploadType.SurfaceTexture) ? mProgramExternal : mProgram2D;
            int texture = (mUploadType == UploadType.SurfaceTexture)
                          ? mSurfaceTextureTextures[textureIndex]
                          : mTextures[textureIndex];
            int offsetTexture = i < 1000 ? mOffsetTexture : mThreadedOffsetTexture;
            if (k > 0)
                glEnable(GL_BLEND);
            else
                glDisable(GL_BLEND);
            drawTile(program, texture, offsetTexture, mScreenWidth, 
                                                      mScreenHeight, i * mTileWidth - mTileWidth / 2 + k * 8,
                                                                     j * mTileHeight - mScrollOffset + k * 8);
                                                                     // j * mTileHeight - 0 + k * 8);
            mDrawStats.endFrame();
        }
    };

    private TileAction mAttachTile = new TileAction() {
        public void DoTile(int i, int j, int k, int textureIndex) {
            // The texture was deleted on the last detach, so glGenTextures a new one and attach to it.
            if (mTextures[textureIndex] != 0)
                return;
            mAttachStats.startFrame();
            glGenTextures(1, mTextures, textureIndex);
            mSurfaceTextures[textureIndex].attachToGLContext(mTextures[textureIndex]);
            mAttachStats.endFrame();
        }
    };

    private TileAction mDetachTile = new TileAction() {
        public void DoTile(int i, int j, int k, int textureIndex) {
            // The texture is deleted during detach, so we assign it to 0.
            if (mTextures[textureIndex] == 0)
                return;
            mDetachStats.startFrame();
            mTextures[textureIndex] = 0;
            mSurfaceTextures[textureIndex].detachFromGLContext();
            mDetachStats.endFrame();
        }
    };

    private TileAction mUpdateTile = new TileAction() {
        public void DoTile(int i, int j, int k, int textureIndex) {
            if (mTextures[textureIndex] == 0)
                return;
            mSurfaceTextures[textureIndex].updateTexImage();
        }
    };


    private TileAction mAttachAndDrawTile = new TileAction() {
        public void DoTile(int i, int j, int k, int textureIndex) {
            mAttachTile.DoTile(i, j, k, textureIndex);
            mDrawTile.DoTile(i, j, k, textureIndex);
        }
    };


    private void iterateOverFrameTiles(TileAction tileAction) {
        int tileStartY = mScrollOffset / mTileHeight;
        for(int k = 0; k < mLayers; k++) {
            for(int i = 0; i < mTileCountX; i++) {
                for(int j = tileStartY; j < tileStartY + mTileCountY; j++) {
                    tileAction.DoTile(i, j, k, textureIndex(i, j, k));
                }
            }
        }
    }

    private void drawTilesForFrame() {
        mDrawBatchStats.startFrame();
        iterateOverFrameTiles(mDrawTile);
        mDrawBatchStats.endFrame();
    }

    private void attachTilesForFrame() {
        Assert.assertTrue(mSurfaceTextures != null);
        mAttachBatchStats.startFrame();
        iterateOverFrameTiles(mAttachTile);
        mAttachBatchStats.endFrame();
    }

    private void detachTilesForFrame() {
        Assert.assertTrue(mSurfaceTextures != null);
        mDetachBatchStats.startFrame();
        iterateOverFrameTiles(mDetachTile);
        mDetachBatchStats.endFrame();
    }

    private void updateTexImageTilesForFrame() {
        Assert.assertTrue(mSurfaceTextures != null);
        mDetachBatchStats.startFrame();
        iterateOverFrameTiles(mUpdateTile);
        mDetachBatchStats.endFrame();
    }

    public void renderFrame() {
        mFrameStats.endFrame();
        mFrameStats.startFrame();

        if (mFramesRendered % 1000 == 0) {
            mFrameStats.logStats("Full Frame Stats");
            LogBatchStats("Draw", mDrawStats, mDrawBatchStats);

            mDrawStats = new FrameStats();
            mDrawBatchStats = new FrameStats();
            mFrameStats = new FrameStats();
        }
/*
        if (mFramesRendered % 50 == 0) {
            mUploadStats.logStats("Upload Stats");
            mUploadStats = new FrameStats();
        }
*/


        if (mUploadType == UploadType.EglImage) {
            initTextures();
            initEglImages();
        }
        if (mUploadType == UploadType.SurfaceTexture) {
            initTextures();
            initSurfaceTextures();
        }

/*
        // Calculate a scroll offset and update scroll offset texture(s)
        int x = 0;
        int y = mScrollOffset;
        int r = x / 256;
        int g = y / 256;
        int b = x % 256;
        int a = y % 256;
        GLUtil.uploadWithTexSubImage(mOffsetTexture, 1, 1, r, g, b, a);
        if (mUploadThread.doneUploading()) {
            mUploadThread.uploadToEglImage(mThreadedOffsetEglImage, r, g, b, a);
            mUploadThread.waitForDoneUploading();
        }
*/


        if (mUploadThread.doneUploading()) {
            if (mUploadType == UploadType.EglImage) {
                mUploadThread.uploadToEglImages(mEglImages, 10000);
            } else if (mUploadType == UploadType.SurfaceTexture) {
                mUploadThread.uploadToSurfaces(mSurfaces, 10000);
            } else {
                throw new RuntimeException("Unsupported upload type.");
            }
        }

        if (mUploadType == UploadType.SurfaceTexture)
            updateTexImageTilesForFrame();

        //attachTilesForFrame();
        //GLUtil.waitSync(mUploadThread.mSync);
        drawTilesForFrame();
        //detachTilesForFrame();

        mScrollOffset += 16;
        mFramesRendered++;
    }

    void GLCHK() {
        int error;
        while ((error = glGetError()) != GL_NO_ERROR) {
            Log.e(TAG, "glError " + error);
            throw new RuntimeException("glError " + error);
        }
    }

    private final String mVertexShader =
        "uniform vec2 screenSize;\n" +
        "uniform vec2 tileSize;\n" +
        "uniform vec2 offset;\n" +
        "attribute vec2 coord;\n" +
        "varying vec2 vTexCoord;\n" +
        "void main() {\n" +
        "  vec2 screenCoord = coord * tileSize + offset; \n" +
        "  vec2 ndcCoord = screenCoord / screenSize * 2.0 - 1.0; \n" +
        "  gl_Position = vec4(ndcCoord, 0.0, 1.0);\n" +
        "  vTexCoord = coord;\n" +
        "}\n";

    private final String mTexOffsetVertexShader =
        "uniform sampler2D offsetTexture;\n" +
        "uniform vec2 screenSize;\n" +
        "uniform vec2 tileSize;\n" +
        "uniform vec2 offset;\n" +
        "attribute vec2 coord;\n" +
        "varying vec2 vTexCoord;\n" +
        "void main() {\n" +
        "  vec4 packedTexOffset = texture2D(offsetTexture, vec2(0,0)) * vec4(255); \n" +
        "  vec2 texOffset = packedTexOffset.xy * vec2(256) + packedTexOffset.zw; \n" +
        "  vec2 screenCoord = coord * tileSize + offset - texOffset; \n" +
        "  vec2 ndcCoord = screenCoord / screenSize * 2.0 - 1.0; \n" +
        "  gl_Position = vec4(ndcCoord, 0.0, 1.0);\n" +
        "  vTexCoord = coord;\n" +
        "}\n";

    private final String mFragmentShader2D =
        "precision mediump float;\n" +
        "varying vec2 vTexCoord;\n" +
        "uniform sampler2D texture;\n" +
        "uniform vec4 test; \n" +
        "void main() {\n" +
        "  gl_FragColor = texture2D(texture, vTexCoord); \n" +
        "}\n";

    private final String mFragmentShaderExternal =
        "#extension GL_OES_EGL_image_external : require \n" +
        "precision mediump float;\n" +
        "varying vec2 vTexCoord;\n" +
        "uniform samplerExternalOES texture;\n" +
        "void main() {\n" +
        "  gl_FragColor = texture2D(texture, vTexCoord); \n" +
        "}\n";

    private int loadShader(int shaderType, String source) {
        int shader = glCreateShader(shaderType);
        glShaderSource(shader, source);
        glCompileShader(shader);
        int[] compiled = new int[1];
        glGetShaderiv(shader, GL_COMPILE_STATUS, compiled, 0);
        Assert.assertTrue(compiled[0] == GL_TRUE);
        return shader;
    }

    private int createProgram(String vertexSource, String fragmentSource) {
        int vertexShader = loadShader(GL_VERTEX_SHADER, vertexSource);
        int pixelShader = loadShader(GL_FRAGMENT_SHADER, fragmentSource);
        int program = glCreateProgram();
        glAttachShader(program, vertexShader);
        glAttachShader(program, pixelShader);
        glLinkProgram(program);
        int[] linkStatus = new int[1];
        glGetProgramiv(program, GL_LINK_STATUS, linkStatus, 0);
        Assert.assertTrue(linkStatus[0] == GL_TRUE);
        return program;
    }

    private class Program {
        public Program(String vertexShader, String fragmentShader, int target) {
            mProgram = createProgram(vertexShader, fragmentShader);
            mScreenSizeHandle = glGetUniformLocation(mProgram, "screenSize");
            mTileSizeHandle = glGetUniformLocation(mProgram, "tileSize");
            mOffsetHandle = glGetUniformLocation(mProgram, "offset");
            mTextureHandle = glGetUniformLocation(mProgram, "texture");
            mOffsetTextureHandle = glGetUniformLocation(mProgram, "offsetTexture");
            mCoordHandle = glGetAttribLocation(mProgram, "coord");
            mTextureTarget = target;
        }
        public int mProgram;
        public int mScreenSizeHandle;
        public int mTileSizeHandle;
        public int mOffsetHandle;
        public int mCoordHandle;
        public int mTextureHandle;
        public int mOffsetTextureHandle;
        public int mTextureTarget;
    }
}
